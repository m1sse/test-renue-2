Ниже описаны команды запуска 3 контейнеров (A, B, C) на 1 сервере, с помощью Docker. 


1. git clone https://gitlab.com/m1sse/test-renue-2.git
2. curl -fsSL https://test.docker.com -o test-docker.sh
3. sudo sh test-docker.sh
4. sudo curl -L "https://github.com/docker/compose/releases/download/v2.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
5. chmod +x /usr/local/bin/docker-compose
6. cd test-renue-2
7. docker-compose up -d --build 
8. docker exec -it server_a_ubuntu bash -c "cat /usr/bin/time_sh.txt"
8.1 При появлении ошибки "cat: /usr/bin/time_sh.txt: No such file or directory" необходимо подождать примерно 1 минуту и повторить команду 8.

Команда "docker-compose up -d --build" запустит 3 контейнера с Ubuntu 20.04. Во время сборки контейнера А на него скопируются плейбуки и bash-скрипт, и после старта все плейбуки выполнятся скриптом. 

Команда "docker exec -it server_a_ubuntu bash -c "cat /usr/bin/time_sh.txt"" выведет в консоль время выполнения атоматизированной части конструкции, а именно выремя выполнения bash-скрипта ansible-start.sh

Итого: сборка контейнеров и запуск - 174s  + время выполнения bash-скрипта - 87s = 261s