FROM ubuntu:20.04 

RUN apt-get update && \
    apt-get install -y ansible && \
    apt-get install -y sshpass && \
    apt-get install -y sudo && \
    ansible-galaxy collection install community.postgresql && \
    ansible-galaxy collection install community.general

USER root

COPY ansible-start.sh /usr/bin
COPY add-user-and-ssh.yml /usr/bin
COPY add-db-and-users-postgresql.yml /usr/bin
COPY iptables.yml /usr/bin
COPY ansible.cfg /usr/bin
COPY test-connect.yml /usr/bin
COPY hosts.yml /usr/bin
COPY id_rsa.pub /usr/bin

ADD ansible-start.sh /usr/bin/ansible-start.sh
RUN chmod +x /usr/bin/ansible-start.sh

#CMD tail -f /dev/null
CMD /usr/bin/ansible-start.sh
