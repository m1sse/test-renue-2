#!/bin/bash
START=$(date +%s)
echo "RUNNING: ansible-playbook -i hosts add-db-and-user-postgresql.yml" 
cd /usr/bin
ansible-playbook -i hosts.yml add-db-and-users-postgresql.yml -u ansible

echo "RUNNING: ansible-playbook -i hosts add-user-and-ssh.yml" 
ansible-playbook -i hosts.yml add-user-and-ssh.yml -u ansible

echo "RUNNING: ansible-playbook -i hosts iptables.yml." 
ansible-playbook -i hosts.yml iptables.yml -u ansible

echo "RUNNING: ansible-playbook -i hosts test-connect.yml" 
ansible-playbook -i hosts.yml test-connect.yml -u ansible
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Время выполнения:  $DIFF seconds" > /usr/bin/time_sh.txt